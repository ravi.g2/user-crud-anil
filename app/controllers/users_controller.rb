# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :set_user, only: %i[show update destroy]
  def show
    render json: @user
  end

  def index
    @users = User.all.page(params[:page]).per_page(params[:per_page])
    render json: @users
  end

  def search
    @users = User.search(params)
    render json: @users
  end

  def create
    user = User.create(user_params)
    if user.save
      render json: user
    else
      render json: user.errors
    end
  end

  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors
    end
  end

  def destroy
    @user.destroy
    head :no_content
  end

  private

  def user_params
    params.require(:user).permit(:firstName, :lastName, :email)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
