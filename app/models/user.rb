# frozen_string_literal: true

class User < ApplicationRecord
  scope :first_name, ->(f_name) { where('first_name LIKE ?', "%#{f_name}%") }
  scope :last_name, ->(l_name) { where('last_name LIKE ?', "%#{l_name}%") }
  scope :email, ->(email) { where('email LIKE ?', "%#{email}%") }
  def self.search(params)
    users = []

    users = User.all
    User.first_name(params[:input]).or(User.last_name(params[:input])).or(User.email(params[:input]))
  end
end
